/*
 *    Copyright 2012 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package net.developerstation.mybatis.caches.redis;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.ibatis.cache.Cache;

/**
 * The Redis-based Cache implementation.
 *
 * @author taox
 */
public class RedisCache implements Cache {
	
	private static final JedisClientWrapper JEDIS_CLIENT_WRAPPER = new JedisClientWrapper();
	
	private String id;
	
	public RedisCache(final String id) {
	  if (id == null) {
	    throw new IllegalArgumentException("Cache instances require an ID");
	  }
	  this.id = id;
	}
	
	public String getId() {
		return this.id;
	}

	public Object getObject(Object key) {
		return JEDIS_CLIENT_WRAPPER.getObject(key, id);
	}

	public ReadWriteLock getReadWriteLock() {
		return null;
	}

	public void putObject(Object key, Object value) {
		JEDIS_CLIENT_WRAPPER.putObject(key, value, id);

	}

	public Object removeObject(Object key) {
		return JEDIS_CLIENT_WRAPPER.removeObject(key, id);
	}
	
	
	public int getSize() {
		return JEDIS_CLIENT_WRAPPER.getKeys(id).size();
	}
	
	public void clear() {
		JEDIS_CLIENT_WRAPPER.clear(id);
	}
	
	public static void main(String[] args) {
		RedisCache cache = new RedisCache("user");
		System.out.println(cache.getObject("mykey"));
		
	}


}
